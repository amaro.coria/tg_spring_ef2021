package net.tecgurus.cuentasbanco;

import net.tecgurus.cuentasbanco.persistence.entities.CatalogoGeneral;
import net.tecgurus.cuentasbanco.persistence.repository.CatalogoGeneralRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class CuentasBancoApplicationTests {

	@Autowired
	private CatalogoGeneralRepository catalogoGeneralRepository;

	@Test
	void testCatalogoGeneral() {
		List<CatalogoGeneral> list = catalogoGeneralRepository.findAll();
		Assertions.assertNotNull(list);
	}

}
