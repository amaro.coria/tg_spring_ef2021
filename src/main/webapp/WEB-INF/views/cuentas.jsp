<%--
  Created by IntelliJ IDEA.
  User: nsl-jamaro
  Date: 03/02/21
  Time: 19:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>

<html>
<head>
    <title>Alta de cuentas en sistema</title>
</head>
<body>
<form:form action="${pageContext.request.contextPath}/procesaCuenta" modelAttribute="formCuentaDTO">
    <table>
        <tr>
            <td><label for="clientes">Selecciona cte</label></td>
            <td>
                <form:select path="idCliente" id="clientes">
                    <form:option value="NONE" label="Select" />
                    <form:options items="${clientes}" itemLabel="nomCte" itemValue="idCte" />
                </form:select>
            </td>
        </tr>
        <tr>
            <td><label for="tiposCuenta">Tipo de cuenta</label></td>
            <td>
                <form:select path="idTipoCuenta" id="tiposCuenta">
                    <form:option value="NONE" label="Select" />
                    <form:options items="${tiposCuenta}" itemLabel="descCompleta" itemValue="idCatalogo" />
                </form:select>
            </td>
        </tr>
        <tr>
            <td><label for="no_cta">No. de cuenta</label></td>
            <td><form:input type="text" id="no_cta" path="noCuenta"></form:input></td>
        </tr>
        <tr>
            <td><label for="fch_alta">Fecha de alta</label></td>
            <td><form:input type="date" id="fch_alta" path="fechaAlta"></form:input></td>
        </tr>
        <tr>
            <td><span><input type="reset" value="Limpiar"></span></td>
            <td><span><input type="submit" value="Enviar"></span></td>
        </tr>
    </table>
</form:form>

</body>
</html>
