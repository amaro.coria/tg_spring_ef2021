<%--
  Created by IntelliJ IDEA.
  User: nsl-jamaro
  Date: 02/02/21
  Time: 19:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"  %>
<html>
<head>
    <title>Clientes</title>
</head>
<body>

<h2>Formulario para dar de alta a un cliente</h2>
<hr />
<div>
    <form:form action="${pageContext.request.contextPath}/procesaFormulario" method="post" modelAttribute="formClienteDTO" >
        <table>
            <tr>
                <td><label for="input_nombre">Nombre</label></td>
                <td><form:input type="text" id="input_nombre" path="nombre" ></form:input></td>
            </tr>
            <tr>
                <td><label for="input_apaterno">Apellido Paterno</label></td>
                <td><form:input type="text" id="input_apaterno" path="apellidoPaterno"></form:input></td>
            </tr>
            <tr>
                <td><label for="input_amaterno">Apellido Materno</label></td>
                <td><form:input type="text" id="input_amaterno" path="apellidoMaterno"></form:input></td>
            </tr>
            <tr>
                <td><label for="input_telefono">Telefono</label></td>
                <td><form:input type="text" id="input_telefono" path="telefono"></form:input></td>
            </tr>
            <tr>
                <td><label for="input_direccion">Direccion</label></td>
                <td><form:input type="text" id="input_direccion" path="direccion"></form:input></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td><span><input type="submit" value="Enviar"/></span></td>
            </tr>
        </table>
    </form:form>
</div>

</body>
</html>
