package net.tecgurus.cuentasbanco.controller;

import net.tecgurus.cuentasbanco.dto.ClienteDTO;
import net.tecgurus.cuentasbanco.dto.FormClienteDTO;
import net.tecgurus.cuentasbanco.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @RequestMapping(value = "/cte")
    public ModelAndView enviaFormulario(){
        ModelAndView mv = new ModelAndView();
        mv.addObject("formClienteDTO", new FormClienteDTO());
        mv.setViewName("clientes");
        return mv;
    }

    @RequestMapping(value = "/procesaFormulario", method = RequestMethod.POST)
    public ModelAndView procesaFormulario(@ModelAttribute("formClienteDTO") FormClienteDTO formClienteDTO){
        ModelAndView modelAndView = new ModelAndView();
        //Bloque para dar de alta a cliente
        ClienteDTO dto = ClienteDTO.builder()
                .apeMatCte(formClienteDTO.getApellidoMaterno())
                .apePatCte(formClienteDTO.getApellidoPaterno())
                .dirCte(formClienteDTO.getDireccion())
                .nomCte(formClienteDTO.getNombre())
                .dirCte(formClienteDTO.getDireccion())
                .telCte(formClienteDTO.getTelefono())
                .build();
        try {
            ClienteDTO saved = clienteService.save(dto);
            modelAndView.addObject("nuevoCte", saved);
            //modelAndView.setViewName();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //Fin de bloque para dar de alta a cliente
        modelAndView.setViewName("tecgurus");
        return modelAndView;
    }

}
