package net.tecgurus.cuentasbanco.controller;

import net.tecgurus.cuentasbanco.dto.ClienteDTO;
import net.tecgurus.cuentasbanco.service.ClienteService;
import net.tecgurus.cuentasbanco.util.API_URI;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = API_URI.API_ENDPOINT_CTE)
public class ClienteRestController {

    private ClienteService clienteService;

    public ClienteRestController(ClienteService clienteService) {
        this.clienteService = clienteService;
    }

    @GetMapping
    public ResponseEntity<List<ClienteDTO>> findAll() throws Exception {
        return ResponseEntity.ok(clienteService.findAll());
    }

}
