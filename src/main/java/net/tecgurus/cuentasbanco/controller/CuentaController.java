package net.tecgurus.cuentasbanco.controller;


import net.tecgurus.cuentasbanco.dto.CatalogoGeneralDTO;
import net.tecgurus.cuentasbanco.dto.ClienteDTO;
import net.tecgurus.cuentasbanco.dto.CuentaDTO;
import net.tecgurus.cuentasbanco.dto.FormCuentaDTO;
import net.tecgurus.cuentasbanco.service.CatalogoGeneralService;
import net.tecgurus.cuentasbanco.service.ClienteService;
import net.tecgurus.cuentasbanco.service.CuentaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.time.Instant;
import java.util.Date;
import java.util.List;

@Controller
public class CuentaController {

    private ClienteService clienteService;
    private CatalogoGeneralService catalogoGeneralService;
    private CuentaService cuentaService;

    public CuentaController(ClienteService clienteService,
                            CatalogoGeneralService catalogoGeneralService,
                            CuentaService cuentaService) {
        this.clienteService = clienteService;
        this.catalogoGeneralService = catalogoGeneralService;
        this.cuentaService = cuentaService;
    }


    @RequestMapping(value = "/cuentas")
    public ModelAndView altaCuenta(ModelAndView modelAndView) throws Exception {
        List<ClienteDTO> clientes = clienteService.findAll();
        List<CatalogoGeneralDTO> tiposCuenta = catalogoGeneralService.findTiposCuenta();
        modelAndView.addObject("clientes", clientes);
        modelAndView.addObject("tiposCuenta", tiposCuenta);
        modelAndView.addObject("formCuentaDTO", new FormCuentaDTO());
        modelAndView.setViewName("cuentas");
        return modelAndView;
    }

    @RequestMapping(value = "/procesaCuenta", method = RequestMethod.POST)
    public ModelAndView procesaCuenta(@ModelAttribute FormCuentaDTO formCuentaDTO) throws Exception {
        ModelAndView modelAndView = new ModelAndView();
        CuentaDTO cuentaDTO = CuentaDTO.builder()
                .noCuenta(formCuentaDTO.getNoCuenta())
                .idEsta(1)
                .idTipoCta(formCuentaDTO.getIdTipoCuenta())
                .fchCrea(Date.from(Instant.now()))
                .build();
        cuentaService.addCuentaToCliente(cuentaDTO, formCuentaDTO.getIdCliente());
        modelAndView.setViewName("tecgurus");
        return modelAndView;
    }

}
