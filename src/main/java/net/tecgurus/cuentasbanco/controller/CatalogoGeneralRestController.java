package net.tecgurus.cuentasbanco.controller;

import net.tecgurus.cuentasbanco.dto.CatalogoGeneralDTO;
import net.tecgurus.cuentasbanco.service.CatalogoGeneralService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static net.tecgurus.cuentasbanco.util.API_URI.API_ENDPOINT_CAT_GRAL;

@RestController
@RequestMapping(value = API_ENDPOINT_CAT_GRAL)
public class CatalogoGeneralRestController {

    private CatalogoGeneralService catalogoGeneralService;

    public CatalogoGeneralRestController(CatalogoGeneralService catalogoGeneralService) {
        this.catalogoGeneralService = catalogoGeneralService;
    }

    @GetMapping
    public ResponseEntity<List<CatalogoGeneralDTO>> findAll() {
        return ResponseEntity.ok(catalogoGeneralService.findAll());
    }

    @PostMapping
    public ResponseEntity<CatalogoGeneralDTO> save(CatalogoGeneralDTO catalogoGeneralDTO) {
        return ResponseEntity.ok(catalogoGeneralService.save(catalogoGeneralDTO));
    }

}
