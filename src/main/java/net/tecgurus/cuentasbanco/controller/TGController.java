package net.tecgurus.cuentasbanco.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
@Slf4j
public class TGController {

    @RequestMapping(value = {"/saluda", "/", "/index"}, method = RequestMethod.GET)
    public ModelAndView saluda(ModelAndView modelAndView, HttpServletRequest request){
        modelAndView.addObject("nombre","JORGE");
        System.out.println(request.getContextPath());
        modelAndView.setViewName("tecgurus");
        return modelAndView;
    }



}
