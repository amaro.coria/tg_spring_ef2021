package net.tecgurus.cuentasbanco.controller;

import net.tecgurus.cuentasbanco.dto.CuentaAsocRequest;
import net.tecgurus.cuentasbanco.dto.CuentaDTO;
import net.tecgurus.cuentasbanco.service.CuentaService;
import net.tecgurus.cuentasbanco.util.API_URI;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = API_URI.API_ENDPOINT_CTA)
public class CuentaRestController {

    private CuentaService cuentaService;

    public CuentaRestController(CuentaService cuentaService) {
        this.cuentaService = cuentaService;
    }

    @GetMapping
    public ResponseEntity<List<CuentaDTO>> findAll() {
        return ResponseEntity.ok(cuentaService.findAll());
    }

    @PostMapping
    public ResponseEntity<CuentaDTO> generateAndAssociate(@RequestBody CuentaAsocRequest request) throws Exception {
        cuentaService.addCuentaToCliente(request.getCuentaDTO(), request.getIdCte());
        return ResponseEntity.ok(request.getCuentaDTO());
    }


}
