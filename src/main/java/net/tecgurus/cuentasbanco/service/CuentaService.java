package net.tecgurus.cuentasbanco.service;

import net.tecgurus.cuentasbanco.dto.CatalogoGeneralDTO;
import net.tecgurus.cuentasbanco.dto.CuentaDTO;
import net.tecgurus.cuentasbanco.persistence.entities.Cuenta;
import net.tecgurus.cuentasbanco.persistence.entities.CuentasCliente;
import net.tecgurus.cuentasbanco.persistence.repository.CuentaRepository;
import net.tecgurus.cuentasbanco.persistence.repository.CuentasClienteRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.time.Instant;
import java.util.List;

@Service
@Transactional
public class CuentaService {
    @Autowired
    private CuentaRepository cuentaRepository;
    @Autowired
    private CuentasClienteRepository cuentasClienteRepository;
    private ModelMapper modelMapper = new ModelMapper();


    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void addCuentaToCliente(CuentaDTO cuenta, Integer idCliente) throws Exception {
        Integer savedCuentaId = generateCuenta(cuenta);
        CuentasCliente cuentasCliente = new CuentasCliente();
            cuentasCliente.setIdCta(savedCuentaId);
            cuentasCliente.setIdCte(idCliente);
            cuentasCliente.setObs("Autom");
            cuentasCliente.setIdEsta(1);
        cuentasClienteRepository.save(cuentasCliente);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public Integer generateCuenta(CuentaDTO cuenta) throws Exception {
        Cuenta entidad = modelMapper.map(cuenta, Cuenta.class);
        entidad.setFchAlta(Date.from(Instant.now()));
        entidad.setFchCrea(Date.from(Instant.now()));
        Integer savedCuentaId = cuentaRepository.save(entidad).getIdCuenta();
        return savedCuentaId;
    }

    public List<CuentaDTO> findAll() {
        List<Cuenta> list = cuentaRepository.findAll();
        List<CuentaDTO> dtos = modelMapper.map(list,  new TypeToken<List<CuentaDTO>>() {}.getType());
        return dtos;
    }


}
