package net.tecgurus.cuentasbanco.service;

import net.tecgurus.cuentasbanco.dto.CatalogoGeneralDTO;
import net.tecgurus.cuentasbanco.persistence.entities.CatalogoGeneral;
import net.tecgurus.cuentasbanco.persistence.repository.CatalogoGeneralRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CatalogoGeneralService {

    private CatalogoGeneralRepository catalogoGeneralRepository;
    @Value("${cuentas.catalogos.tipoCuenta}")
    private String tipoCta;
    @Autowired
    private ModelMapper modelMapper;


    public CatalogoGeneralService(CatalogoGeneralRepository catalogoGeneralRepository) {
        this.catalogoGeneralRepository = catalogoGeneralRepository;
    }

    public List<CatalogoGeneralDTO> findTiposCuenta() {
        List<CatalogoGeneral> list = catalogoGeneralRepository.findAllByDescCorta(tipoCta);
        List<CatalogoGeneralDTO> dtos = modelMapper.map(list,  new TypeToken<List<CatalogoGeneralDTO>>() {}.getType());
        return  dtos;
    }

    public List<CatalogoGeneralDTO> findAll() {
        List<CatalogoGeneral> list = catalogoGeneralRepository.findAll();
        List<CatalogoGeneralDTO> dtos = modelMapper.map(list,  new TypeToken<List<CatalogoGeneralDTO>>() {}.getType());
        return dtos;
    }

    public CatalogoGeneralDTO save(CatalogoGeneralDTO dto) {
        CatalogoGeneral entity = modelMapper.map(dto, CatalogoGeneral.class);
        entity = catalogoGeneralRepository.save(entity);
        dto = modelMapper.map(entity, CatalogoGeneralDTO.class);
        return dto;
    }


}
