package net.tecgurus.cuentasbanco.service;

import net.tecgurus.cuentasbanco.dto.ClienteDTO;
import net.tecgurus.cuentasbanco.persistence.entities.Cliente;
import net.tecgurus.cuentasbanco.persistence.repository.ClienteRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public List<ClienteDTO> findAll() throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        List<Cliente> clienteList = clienteRepository.findAll();
        List<ClienteDTO> list = modelMapper.map(clienteList,  new TypeToken<List<Cliente>>() {}.getType());
        return list;
    }

    public ClienteDTO findById(Integer id) throws Exception {
        ModelMapper modelMapper = new ModelMapper();
       Cliente cte = clienteRepository.findById(id).get();
       ClienteDTO dto = modelMapper.map(cte, ClienteDTO.class);
       return dto;
    }

    public ClienteDTO save(ClienteDTO clienteDTO) throws Exception {
        ModelMapper modelMapper = new ModelMapper();
        Cliente cte = modelMapper.map(clienteDTO, Cliente.class);
        Cliente saved = clienteRepository.save(cte);
        ClienteDTO dtoSaved = modelMapper.map(saved, ClienteDTO.class);
        return dtoSaved;
    }

}
