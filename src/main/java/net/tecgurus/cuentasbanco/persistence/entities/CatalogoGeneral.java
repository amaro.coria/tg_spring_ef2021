package net.tecgurus.cuentasbanco.persistence.entities;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "catalogo_general")
@Data
public class CatalogoGeneral implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cat")
    private Integer idCatalogo;
    @Column(name = "dsc_cor_cat")
    private String descCorta;
    @Column(name = "dsc_com_cat")
    private String descCompleta;
    @Column(name = "id_est_cat")
    private Integer idEstCatalogo;
    @Column(name = "fch_crea")
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;
    @Column(name = "fch_mod")
    @Temporal(TemporalType.DATE)
    private Date fechaActualizacion;

}
