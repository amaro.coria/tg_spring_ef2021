package net.tecgurus.cuentasbanco.persistence.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "movimientos_cuenta")
public class MovimientosCuenta {
    private Integer idMovCta;
    private Timestamp fchCrea;
    private BigDecimal mntMov;
    private String dscMov;
    private String folAut;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_mov_cta", nullable = false)
    public Integer getIdMovCta() {
        return idMovCta;
    }

    public void setIdMovCta(Integer idMovCta) {
        this.idMovCta = idMovCta;
    }

    @Basic
    @Column(name = "fch_crea", nullable = false)
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "mnt_mov", nullable = false, precision = 2)
    public BigDecimal getMntMov() {
        return mntMov;
    }

    public void setMntMov(BigDecimal mntMov) {
        this.mntMov = mntMov;
    }

    @Basic
    @Column(name = "dsc_mov", nullable = true, length = 50)
    public String getDscMov() {
        return dscMov;
    }

    public void setDscMov(String dscMov) {
        this.dscMov = dscMov;
    }

    @Basic
    @Column(name = "fol_aut", nullable = false, length = 20)
    public String getFolAut() {
        return folAut;
    }

    public void setFolAut(String folAut) {
        this.folAut = folAut;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MovimientosCuenta that = (MovimientosCuenta) o;
        return Objects.equals(idMovCta, that.idMovCta) && Objects.equals(fchCrea, that.fchCrea) && Objects.equals(mntMov, that.mntMov) && Objects.equals(dscMov, that.dscMov) && Objects.equals(folAut, that.folAut);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMovCta, fchCrea, mntMov, dscMov, folAut);
    }
}
