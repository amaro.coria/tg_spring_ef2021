package net.tecgurus.cuentasbanco.persistence.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cliente")
public class Cliente {
    private Integer idCte;
    private String nomCte;
    private String apePatCte;
    private String apeMatCte;
    private String telCte;
    private String dirCte;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cte", nullable = false)
    public Integer getIdCte() {
        return idCte;
    }

    public void setIdCte(Integer idCte) {
        this.idCte = idCte;
    }

    @Basic
    @Column(name = "nom_cte", nullable = false, length = 50)
    public String getNomCte() {
        return nomCte;
    }

    public void setNomCte(String nomCte) {
        this.nomCte = nomCte;
    }

    @Basic
    @Column(name = "ape_pat_cte", nullable = false, length = 50)
    public String getApePatCte() {
        return apePatCte;
    }

    public void setApePatCte(String apePatCte) {
        this.apePatCte = apePatCte;
    }

    @Basic
    @Column(name = "ape_mat_cte", nullable = true, length = 50)
    public String getApeMatCte() {
        return apeMatCte;
    }

    public void setApeMatCte(String apeMatCte) {
        this.apeMatCte = apeMatCte;
    }

    @Basic
    @Column(name = "tel_cte", nullable = true, length = 20)
    public String getTelCte() {
        return telCte;
    }

    public void setTelCte(String telCte) {
        this.telCte = telCte;
    }

    @Basic
    @Column(name = "dir_cte", nullable = true, length = 255)
    public String getDirCte() {
        return dirCte;
    }

    public void setDirCte(String dirCte) {
        this.dirCte = dirCte;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cliente cliente = (Cliente) o;
        return Objects.equals(idCte, cliente.idCte) && Objects.equals(nomCte, cliente.nomCte) && Objects.equals(apePatCte, cliente.apePatCte) && Objects.equals(apeMatCte, cliente.apeMatCte) && Objects.equals(telCte, cliente.telCte) && Objects.equals(dirCte, cliente.dirCte);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCte, nomCte, apePatCte, apeMatCte, telCte, dirCte);
    }
}
