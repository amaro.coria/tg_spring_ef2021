package net.tecgurus.cuentasbanco.persistence.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.Objects;

public class CuentasClientePK implements Serializable {
    private Integer idCta;
    private Integer idCte;

    @Column(name = "id_cta", nullable = false)
    @Id
    public Integer getIdCta() {
        return idCta;
    }

    public void setIdCta(Integer idCta) {
        this.idCta = idCta;
    }

    @Column(name = "id_cte", nullable = false)
    @Id
    public Integer getIdCte() {
        return idCte;
    }

    public void setIdCte(Integer idCte) {
        this.idCte = idCte;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CuentasClientePK that = (CuentasClientePK) o;
        return Objects.equals(idCta, that.idCta) && Objects.equals(idCte, that.idCte);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCta, idCte);
    }
}
