package net.tecgurus.cuentasbanco.persistence.entities;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "cuenta")
public class Cuenta implements Serializable {

    @Id
    @Column(name = "id_cta")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idCuenta;
    @Column(name = "no_cta")
    private String noCuenta;
    @Column(name = "id_tipo_cta")
    private Integer idTipoCta; //TODO revisar en sig sesion el one to many - mano to one
    @Column(name = "fch_alta")
    @Temporal(TemporalType.DATE)
    private Date fchAlta;
    @Column(name = "id_esta")
    private Integer idEsta;
    @Column(name = "fch_crea")
    @Temporal(TemporalType.DATE)
    private Date fchCrea;
    @Column(name = "fch_modi")
    @Temporal(TemporalType.DATE)
    private Date fchModi;

    public Integer getIdCuenta() {
        return idCuenta;
    }

    public void setIdCuenta(Integer idCuenta) {
        this.idCuenta = idCuenta;
    }

    public String getNoCuenta() {
        return noCuenta;
    }

    public void setNoCuenta(String noCuenta) {
        this.noCuenta = noCuenta;
    }

    public Integer getIdTipoCta() {
        return idTipoCta;
    }

    public void setIdTipoCta(Integer idTipoCta) {
        this.idTipoCta = idTipoCta;
    }

    public Date getFchAlta() {
        return fchAlta;
    }


    public void setFchAlta(Date fchAlta) {
        this.fchAlta = fchAlta;
    }

    public Integer getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(Integer idEsta) {
        this.idEsta = idEsta;
    }

    public Date getFchCrea() {
        return fchCrea;
    }


    public void setFchCrea(Date fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "fch_modi", nullable = true)
    public Date getFchModi() {
        return fchModi;
    }

    public void setFchModi(Date fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cuenta cuenta = (Cuenta) o;
        return idCuenta.equals(cuenta.idCuenta) && noCuenta.equals(cuenta.noCuenta);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCuenta, noCuenta);
    }

    @Override
    public String toString() {
        return "Cuenta{" +
                "idCuenta=" + idCuenta +
                ", noCuenta='" + noCuenta + '\'' +
                ", idTipoCta=" + idTipoCta +
                ", fchAlta=" + fchAlta +
                ", idEsta=" + idEsta +
                ", fchCrea=" + fchCrea +
                ", fchModi=" + fchModi +
                '}';
    }

}
