package net.tecgurus.cuentasbanco.persistence.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "cuentas_cliente")
@IdClass(CuentasClientePK.class)
public class CuentasCliente {
    private Integer idCta;
    private Integer idCte;
    private String obs;

    @Column(name = "id_esta")
    public Integer getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(Integer idEsta) {
        this.idEsta = idEsta;
    }

    private Integer idEsta;

    @Id
    @Column(name = "id_cta", nullable = false)
    public Integer getIdCta() {
        return idCta;
    }

    public void setIdCta(Integer idCta) {
        this.idCta = idCta;
    }

    @Id
    @Column(name = "id_cte", nullable = false)
    public Integer getIdCte() {
        return idCte;
    }

    public void setIdCte(Integer idCte) {
        this.idCte = idCte;
    }

    @Basic
    @Column(name = "obs", nullable = true, length = 50)
    public String getObs() {
        return obs;
    }

    public void setObs(String obs) {
        this.obs = obs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CuentasCliente that = (CuentasCliente) o;
        return Objects.equals(idCta, that.idCta) && Objects.equals(idCte, that.idCte) && Objects.equals(obs, that.obs);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCta, idCte, obs);
    }
}
