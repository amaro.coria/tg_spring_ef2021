package net.tecgurus.cuentasbanco.persistence.repository;

import net.tecgurus.cuentasbanco.persistence.entities.CuentasCliente;
import net.tecgurus.cuentasbanco.persistence.entities.CuentasClientePK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CuentasClienteRepository extends JpaRepository<CuentasCliente, CuentasClientePK> {
}
