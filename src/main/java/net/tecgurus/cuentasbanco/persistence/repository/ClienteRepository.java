package net.tecgurus.cuentasbanco.persistence.repository;

import net.tecgurus.cuentasbanco.persistence.entities.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ClienteRepository extends JpaRepository<Cliente, Integer> {
}
