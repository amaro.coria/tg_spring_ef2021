package net.tecgurus.cuentasbanco.persistence.repository;

import net.tecgurus.cuentasbanco.persistence.entities.Cuenta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CuentaRepository extends JpaRepository<Cuenta, Integer> {
}
