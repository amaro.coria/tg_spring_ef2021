package net.tecgurus.cuentasbanco.persistence.repository;

import net.tecgurus.cuentasbanco.persistence.entities.MovimientosCuenta;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MovimientosCuentaRepository extends JpaRepository<MovimientosCuenta, Integer> {
}
