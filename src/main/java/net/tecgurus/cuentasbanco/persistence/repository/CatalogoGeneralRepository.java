package net.tecgurus.cuentasbanco.persistence.repository;

import net.tecgurus.cuentasbanco.persistence.entities.CatalogoGeneral;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CatalogoGeneralRepository extends JpaRepository<CatalogoGeneral, Integer> {

    List<CatalogoGeneral> findAllByDescCorta(String descCorta);


}