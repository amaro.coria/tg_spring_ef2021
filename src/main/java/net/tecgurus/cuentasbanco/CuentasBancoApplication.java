package net.tecgurus.cuentasbanco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
public class CuentasBancoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CuentasBancoApplication.class, args);
	}

}
