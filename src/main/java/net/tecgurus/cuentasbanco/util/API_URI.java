package net.tecgurus.cuentasbanco.util;

public class API_URI {

    public static final String API_VERSION = "/api/v1";
    public static final String API_ENDPOINT_CAT_GRAL = API_VERSION + "/cat";
    public static final String API_ENDPOINT_CTA = API_VERSION + "/cta";
    public static final String API_ENDPOINT_CTE = API_VERSION + "/cte";

}
