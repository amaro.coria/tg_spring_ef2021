package net.tecgurus.cuentasbanco.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class FormClienteDTO implements Serializable {

    private String nombre;
    private String apellidoPaterno;
    private String apellidoMaterno;
    private String telefono;
    private String direccion;

}
