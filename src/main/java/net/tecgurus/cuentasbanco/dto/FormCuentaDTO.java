package net.tecgurus.cuentasbanco.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Data
public class FormCuentaDTO implements Serializable {

    private String noCuenta;
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date fechaAlta;
    private Integer idCliente;
    private Integer idTipoCuenta;

}
