package net.tecgurus.cuentasbanco.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CuentaDTO implements Serializable {
    private Integer idCuenta;
    private String noCuenta;
    private Integer idTipoCta;
    private String idTipoCtaDesc;
    private Integer idEsta;
    private String idEstaDsc;
    private Date fchCrea;
}
