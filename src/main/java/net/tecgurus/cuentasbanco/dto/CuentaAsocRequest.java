package net.tecgurus.cuentasbanco.dto;

import lombok.*;

import java.io.Serializable;

@Value
public class CuentaAsocRequest  implements Serializable {

    private CuentaDTO cuentaDTO;
    private Integer idCte;

}
