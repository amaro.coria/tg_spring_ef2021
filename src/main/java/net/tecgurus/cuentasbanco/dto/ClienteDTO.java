package net.tecgurus.cuentasbanco.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClienteDTO implements Serializable {
    private Integer idCte;
    private String nomCte;
    private String apePatCte;
    private String apeMatCte;
    private String telCte;
    private String dirCte;
}
