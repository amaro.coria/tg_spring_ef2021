package net.tecgurus.cuentasbanco.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CatalogoGralDTO implements Serializable {
    private Integer idCatalogo;
    private String descCorta;
    private String descCompleta;
    private Integer idEstCatalogo;

}
